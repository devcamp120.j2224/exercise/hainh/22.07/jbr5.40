package com.example.demo.Service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.Model.Customer;
import com.example.demo.Model.Invoices;

@Service
public class InvoiceService {
    // có 2 cách để set Customer 
    // 1 đặt biến static cho customer1 và 3 , thì gọi thẳng class CustomerService. 1 , 3
    // 2 ko cần đặt biến static , đặt autowired gọi đến class CustomerService tạo 1 đối tượng , rồi chấm với customer2
    @Autowired
    private CustomerService customerService ;

    public ArrayList<Invoices> getInvoiceList(){
        ArrayList<Invoices> invoices = new ArrayList<Invoices>();;

        Invoices invoice1 = new Invoices(1 , new Customer(1 , "Hải" , 10 ) , 10.0 );
        Invoices invoice2 = new Invoices(2 , new Customer(1 , "Hoàng Hải" , 20 ) , 20.0 );
        Invoices invoice3 = new Invoices(3 , new Customer(1 , "Nguyễn Hải" , 30 ) , 30.0 );


        invoice1.setCustomer(CustomerService.customer1);
        invoice2.setCustomer(customerService.customer2);
        invoice3.setCustomer(CustomerService.customer3);

        invoices.add(invoice1);
        invoices.add(invoice2);
        invoices.add(invoice3);

        return invoices;
    }
}
