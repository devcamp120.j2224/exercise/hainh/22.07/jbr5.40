package com.example.demo.Service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.example.demo.Model.Customer;

@Service
public class CustomerService {
    
    static Customer customer1 = new Customer(1 , "Hải" ,10);
    Customer customer2 = new Customer(1 , "Hoàng Hải" ,20);
    static Customer customer3 = new Customer(1 , "Nguyễn Hải" ,30);

    public ArrayList<Customer> getCustomerList() {
        ArrayList<Customer> customers = new ArrayList<Customer>();

        customers.add(customer1);
        customers.add(customer2);
        customers.add(customer3);

        return customers;

    }
}
